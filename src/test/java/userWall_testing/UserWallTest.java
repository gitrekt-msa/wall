package userWall_testing;

import static org.junit.Assert.assertEquals;


import com.gitrekt.quora.database.postgres.handlers.WallPostgresHandler;
import com.gitrekt.quora.models.Wall;
import org.junit.Before;

import org.junit.Test;

import java.util.List;


public class UserWallTest {

    protected WallPostgresHandler handler;

    @Before
    public void init() {
        handler = new WallPostgresHandler();

    }


    @Test
    public void checkFollowersQuestions() {

        String userId = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";
        List<Wall> wall = handler.viewQuestions(userId);
        System.out.println("wall: " + wall.size());

        String question1Id = "5f7d5b2c-ea5a-4c52-bb11-af70e16f3649";
        String question2Id = "7b33e4fc-1a41-4661-a4d9-563fc21ce90f";

        assertEquals("The Follower questions size is not correct",2 , wall.size());
        assertEquals("The Follower first question Id is not correct", question1Id, wall.get(0).getQuestionId());
        assertEquals("The Follower second question Id is not correct",question2Id ,wall.get(1).getQuestionId());

    }

    @Test
    public void checkFollowersAnswers() {

        String userId = "0d50fcd3-eed6-4774-8ac0-42c0c2b72b0d";
        String question1Id = "5f7d5b2c-ea5a-4c52-bb11-af70e16f3649";
        String question2Id = "5f7d5b2c-ea5a-4c52-bb11-af70e16f3648";

        List<Wall> wall = handler.viewAnswers(userId);
        System.out.println("wall: " + wall.size());

        for(int i=0;i<wall.size();i++){
            System.out.println(wall.get(i));
        }

        assertEquals("The Follower answer size is not correct", 2, wall.size());
        assertEquals("The Follower answers for the correct first question Id is not correct", question1Id, wall.get(0).getQuestionId());
        assertEquals("The Follower answers for the correct second question Id is not correct", question2Id, wall.get(1).getQuestionId());
    }


}



