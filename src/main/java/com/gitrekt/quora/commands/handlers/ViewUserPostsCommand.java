package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.WallPostgresHandler;
import com.gitrekt.quora.models.Wall;
import com.gitrekt.quora.queue.MessageQueueConnection;
import com.gitrekt.quora.queue.MessageQueueConsumer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class ViewUserPostsCommand extends Command {

  private static final String[] argumentNames = new String[] {"userId"};

  private static final Logger LOGGER = Logger.getLogger(MessageQueueConsumer.class.getName());

  private final BlockingQueue<HashMap<String, Object>> queue = new ArrayBlockingQueue<>(1);

  public ViewUserPostsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);
    String userId = (String) args.get("userId");
    List<Wall> wallQuestions = ((WallPostgresHandler) postgresHandler).viewQuestions(userId);
    List<Wall> wallAnswers = ((WallPostgresHandler) postgresHandler).viewAnswers(userId);

    String correlationId = UUID.randomUUID().toString();

    Gson gson = new GsonBuilder().create();
    JsonElement questions = gson.toJsonTree(wallQuestions, List.class);
    JsonElement answers = gson.toJsonTree(wallAnswers, List.class);

    JsonObject jsonObject = new JsonObject();
    jsonObject.add("Follower Questions", questions);
    jsonObject.add("Follower Answers", answers);

    MessageQueueConsumer.getInstance()
        .addListener(
            correlationId,
            json -> {
              try {
                HashMap<String, Object> arguments =
                    new Gson().fromJson(json.get("response").toString(), HashMap.class);

                queue.put(arguments);
              } catch (Exception exception) {
                System.out.println(exception);
              }
            });

    getRecommandations(userId, correlationId);

    HashMap<String, Object> arguments;

    arguments = queue.poll(8, TimeUnit.SECONDS);

    //arguments = queue.take();

    System.out.println("args" + arguments);

    if (arguments != null) {
      jsonObject.add("Recommended Topics", gson.toJsonTree(arguments.get("topics"), List.class));
      jsonObject.add(
          "Recommended Questions", gson.toJsonTree(arguments.get("questions"), List.class));
      jsonObject.add("Recommended Users", gson.toJsonTree(arguments.get("users"), List.class));
    }
    JsonElement result = gson.fromJson(jsonObject.toString(), JsonElement.class);
    return result;
  }

  /**
   * call allRecommandtions from recommand-micro-service.
   *
   * @param userId userId of the user
   * @param correlationId id of the request
   */
  public static void getRecommandations(String userId, String correlationId) {

    try {

      final AMQP.BasicProperties replyProperties =
          new AMQP.BasicProperties.Builder()
              .replyTo("wall-v1-queue")
              .correlationId(correlationId)
              .build();
      JsonObject response = new JsonObject();
      response.addProperty("command", "allRecommend");
      response.addProperty("userId", userId);

      Channel channel = MessageQueueConnection.getInstance().createChannel();
      channel.basicPublish(
          "",
          "recommend-v1-queue",
          replyProperties,
          response.toString().getBytes(StandardCharsets.UTF_8));
      channel.close();
    } catch (IOException | TimeoutException exception) {
      LOGGER.severe(
          String.format("Error sending the response to main server\n%s", exception.getMessage()));
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }
}
