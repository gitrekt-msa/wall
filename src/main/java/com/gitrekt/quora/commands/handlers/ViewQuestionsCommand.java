package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.database.postgres.handlers.WallPostgresHandler;
import com.gitrekt.quora.models.Wall;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.List;

public class ViewQuestionsCommand extends Command {
  private static final String[] argumentNames = new String[]{"userId"};

  public ViewQuestionsCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);
    String userId = (String) args.get("userId");
    List<Wall> wallQuestions = ((WallPostgresHandler)postgresHandler).viewQuestions(userId);
    Gson gson = new GsonBuilder().create();
    JsonElement questions = gson.toJsonTree(wallQuestions, List.class);
    return questions;
  }

}
