package com.gitrekt.quora.database.postgres.handlers;

import com.gitrekt.quora.models.Wall;

import java.sql.SQLException;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class WallPostgresHandler extends PostgresHandler<Wall> {

  public WallPostgresHandler() {
    super("public.wall", Wall.class);
  }

  private final int postsNumber = 5;

  /**
   * Query for getting the questions asked by users I follow to be viewed on the wall.
   *
   * @param userId userID for the user
   */
  public List<Wall> viewQuestions(String userId) {

    try {
      ResultSetHandler<List<Wall>> elements =
          new BeanListHandler<Wall>(mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<Wall> wall =
          runner.query(String.format("SELECT * FROM View_questions('" + userId + "')"), elements);
      return wall.stream().limit(postsNumber).collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }

  /**
   * Query for getting the answers answered by users I follow to be viewed on the wall.
   *
   * @param userId userID for the user
   */
  public List<Wall> viewAnswers(String userId) {
    try {
      ResultSetHandler<List<Wall>> elements =
          new BeanListHandler<Wall>(mapper, new BasicRowProcessor(new GenerousBeanProcessor()));
      List<Wall> wall =
          runner.query(String.format("SELECT * FROM View_answers('" + userId + "')"), elements);
      return wall.stream().limit(postsNumber).collect(Collectors.toList());
    } catch (SQLException exception) {
      exception.printStackTrace();
    }
    return null;
  }

  /**
   * Testing Wall function. 1 - view Questions of the users I follow. 2- view Answers of the users I
   * follow
   *
   * @param args Empty.
   */
  public static void main(String[] args) {

    WallPostgresHandler wallPostgresHandler = new WallPostgresHandler();
    List<Wall> wall = wallPostgresHandler.viewAnswers("7b33e4fc-1a41-4661-a4d9-563fc21cd89e");
    System.out.println("Size of the Answers: " + wall.size());
    for (int i = 0; i < wall.size(); i++) {
      System.out.println(i + " : " + wall.get(i));
    }
  }
}
