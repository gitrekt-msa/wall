package com.gitrekt.quora.models;

public class Wall {
  private String questionId;
  private String userId;
  private String firstName;
  private String lastName;
  private String title;
  private String body;
  private int upvotes;
  private int subscribers;
  private String answerId;
  private String answerUserId;
  private String answerFirstName;
  private String answerLastName;
  private String answerText;

  public String getQuestionId() {
    return questionId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public int getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(int upvotes) {
    this.upvotes = upvotes;
  }

  public int getSubscribers() {
    return subscribers;
  }

  public void setSubscribers(int subscribers) {
    this.subscribers = subscribers;
  }

  public String getAnswerId() {
    return answerId;
  }

  public void setAnswerId(String answerId) {
    this.answerId = answerId;
  }

  public String getAnswerUserId() {
    return answerUserId;
  }

  public void setAnswerUserId(String answerUserId) {
    this.answerUserId = answerUserId;
  }

  public String getAnswerFirstName() {
    return answerFirstName;
  }

  public void setAnswerFirstName(String answerFirstName) {
    this.answerFirstName = answerFirstName;
  }

  public String getAnswerLastName() {
    return answerLastName;
  }

  public void setAnswerLastName(String answerLastName) {
    this.answerLastName = answerLastName;
  }

  public String getAnswerText() {
    return answerText;
  }

  public void setAnswerText(String answerText) {
    this.answerText = answerText;
  }

  @Override
  public String toString() {
    return "Wall{"
        + "questionId='"
        + questionId
        + '\''
        + ", userId='"
        + userId
        + '\''
        + ", firstName='"
        + firstName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", title='"
        + title
        + '\''
        + ", body='"
        + body
        + '\''
        + ", upvotes="
        + upvotes
        + ", subscribers="
        + subscribers
        + ", answerId='"
        + answerId
        + '\''
        + ", answerUserId='"
        + answerUserId
        + '\''
        + ", answerFirstName='"
        + answerFirstName
        + '\''
        + ", answerLastName='"
        + answerLastName
        + '\''
        + ", answerText='"
        + answerText
        + '\''
        + '}';
  }
}
