package com.gitrekt.quora.queue;

import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueInstance {

  private static BlockingQueue<HashMap<String, Object>> queue;

  /**
   * Returns the Singleton Instance.
   *
   * @return The Blocking Queue Instance
   */
  public static BlockingQueue getInstance() {
    if (queue != null) {
      return queue;
    }
    synchronized (BlockingQueueInstance.class) {
      if (queue == null) {
        queue = new ArrayBlockingQueue<>(10);
      }
    }
    return queue;
  }
}
