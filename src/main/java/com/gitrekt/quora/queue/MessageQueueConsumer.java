package com.gitrekt.quora.queue;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.controller.Invoker;
import com.gitrekt.quora.exceptions.AuthenticationException;
import com.gitrekt.quora.pooling.ThreadPool;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.logging.Logger;

/** Represents a consumer that consumes from the micro-service's queue. */
/** Represents a consumer that consumes from the micro-service's queue. */
public class MessageQueueConsumer {

  private static final Logger LOGGER = Logger.getLogger(MessageQueueConsumer.class.getName());

  private static MessageQueueConsumer instance;

  private ConcurrentMap<String, Consumer<JsonObject>> listeners;

  /** Channel to the RabbitMQ service. */
  private Channel channel;

  /**
   * Creates a Message Queue Consumer.
   *
   * <ol>
   *   <li>1. Creating a channel.
   *   <li>2. Declaring a Queue.
   *   <li>3. Creating and Adding the Consumer to the queue.
   * </ol>
   *
   * @throws IOException if an error occurred creating either the Channel or Queue, or when adding
   *     the Consumer.
   */
  public MessageQueueConsumer() throws IOException {
    final String queueName = System.getenv("QUEUE_NAME");

    /*
     * Maps all listeners using the Correlation ID.
     */
    listeners = new ConcurrentHashMap<>();

    channel = MessageQueueConnection.getInstance().createChannel();

    /*
     * Declare a queue that is persistent/durable.
     */
    channel.queueDeclare(queueName, true, false, false, null);

    /*
     * Consume messages from the queue, acknowledge when
     * messages are sent to the consumer.
     */
    channel.basicConsume(queueName, true, createConsumer());
  }

  /**
   * Creates a consumer that will consume from the queue.
   *
   * @return The consumer
   */
  private DefaultConsumer createConsumer() {
    /*
     * Simple consumer that logs the message.
     */
    return new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(
          String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) {

        String correlationId = properties.getCorrelationId();
        JsonObject messageJson =
            new JsonParser().parse(new String(body, StandardCharsets.UTF_8)).getAsJsonObject();

        LOGGER.info(
            String.format(
                "Consuming the received message (%s) with correlationId %s.",
                messageJson, correlationId));

        Consumer<JsonObject> consumer = listeners.get(correlationId);
        if (consumer != null) {
          try {
            consumer.accept(messageJson);
            if (messageJson.get("command") == null) {
              return;
            }
          } catch (IllegalStateException exception) {
            // pass
          } catch (Exception exception) {
            LOGGER.info("Error calling listener");
            exception.printStackTrace();
          } finally {
            listeners.remove(correlationId);
          }
        }

        Runnable runnable =
            () -> {
              Class myClass = null;
              Command command = null;
              String path = "com.gitrekt.quora.commands.handlers.";

              String replyTo = properties.getReplyTo();
              System.out.println("Reply to : " + replyTo);
              AMQP.BasicProperties replyProperties =
                  new AMQP.BasicProperties.Builder()
                      .correlationId(properties.getCorrelationId())
                      .build();
              JsonObject response = new JsonObject();

              try {

                if (messageJson.get("command") == null) {
                  return;
                }
                String commandName = messageJson.get("command").getAsString();

                HashMap<String, Object> arguments = new HashMap<>();

                if (!messageJson.get("isAuthenticated").getAsBoolean()) {
                  throw new AuthenticationException("You are not authorized");
                }

                if (messageJson.get("userId") != null) {
                  arguments.put("userId", messageJson.get("userId").getAsString());
                }

                Object result = Invoker.invoke(commandName, arguments);

                JsonElement resultJson = (JsonElement) result;

                response.addProperty("statusCode", "200");
                response.add("response", resultJson);

              } catch (AuthenticationException authException) {
                authException.printStackTrace();
                response.addProperty(
                    "statusCode", String.valueOf(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()));
                response.addProperty("error", authException.getMessage());
              } catch (Exception exception) {
                exception.printStackTrace();
                response.addProperty(
                    "statusCode", String.valueOf(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()));
                response.addProperty("error", exception.getMessage());
              }

              try {
                Channel channel = MessageQueueConnection.getInstance().createChannel();
                channel.basicPublish(
                    "",
                    replyTo,
                    replyProperties,
                    response.toString().getBytes(StandardCharsets.UTF_8));
                channel.close();
              } catch (IOException | TimeoutException exception) {
                LOGGER.severe(
                    String.format(
                        "Error sending the response to main server\n%s", exception.getMessage()));
              }
            };

        ThreadPool.getInstance().run(runnable);
      }
    };
  }

  public void addListener(String correlationId, Consumer<JsonObject> consumer) {
    listeners.put(correlationId, consumer);
  }

  /** Closes the RabbitMQ Channel. */
  public void close() throws IOException, TimeoutException {
    channel.close();
  }

  /**
   * Returns the Singleton Instance.
   *
   * @return The Message Queue Consumer Instance
   * @throws IOException If an error occurred
   * @throws TimeoutException If an error occurred
   */
  public static MessageQueueConsumer getInstance() throws IOException, TimeoutException {
    if (instance != null) {
      return instance;
    }
    synchronized (MessageQueueConsumer.class) {
      if (instance == null) {
        instance = new MessageQueueConsumer();
      }
    }
    return instance;
  }
}
