# Wall Micro-Service

This is the timeline of a user which is responsible for getting recommended questions, topics, and users in addition to questions and answers for users who I follow.

### Instructions

1. Clone the repo 

2. Run `docker-compose up -d` to start the server and all associated services

3. Access the server [http://localhost:3000](http://localhost:3000)